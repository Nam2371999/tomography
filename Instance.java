import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Instance {
	public final ArrayList<ArrayList<Integer>> lineSequences;
	public final ArrayList<ArrayList<Integer>> columnSequences;
	public final int nbLines;
	public final int nbColumns;
	public final Color[][] grid;

	/*
		An instance of the problem
	*/
	public Instance(int numInstance) throws FileNotFoundException, IOException {
		lineSequences = new ArrayList<ArrayList<Integer>>();
		columnSequences = new ArrayList<ArrayList<Integer>>();

		readInstance(numInstance);

		nbLines = lineSequences.size();
		nbColumns = columnSequences.size();

		grid = new Color[nbLines][nbColumns];
		for (int i = 0; i < nbLines; i++) {
			for (int j = 0; j < nbColumns; j++) {
				grid[i][j] = Color.NOT_COLORED;
			}
		}
	}

	/*
		Read the problem instance from a file
	*/
	public void readInstance(int numInstance) throws FileNotFoundException, IOException {
		String filename = "./instances/" + numInstance + ".txt";

		BufferedReader in = new BufferedReader(new FileReader(filename));

		String line;

		while (!(line = in.readLine()).contains("#")) {
			if (line.isEmpty()) {
				lineSequences.add(new ArrayList<Integer>());
			} else {
				lineSequences.add(Arrays.stream(line.split(" ")).map(Integer::parseInt).collect(Collectors.toCollection(ArrayList<Integer>::new)));
			}
		};

		while ((line = in.readLine()) != null) {
			if (line.isEmpty()) {
				columnSequences.add(new ArrayList<Integer>());
			} else {
				columnSequences.add(Arrays.stream(line.split(" ")).map(Integer::parseInt).collect(Collectors.toCollection(ArrayList<Integer>::new)));
			}
		};
		
		in.close();
	}

	public void solve() {
		//displayInstance();

		Result res = enumeration();

		if (res == Result.SUCCEEDED) {
			System.out.println("Coloration successful");
		}
		else {
			System.out.println("Coloration impossible.");
		}

		displayGrid(grid);
		System.out.println(countRec);
	}

	/*
		Display instance
	*/
	public void displayInstance() {
		System.out.println("Instance with " + lineSequences.size() + " lines and " + columnSequences.size() + " columns.");
		System.out.println("Line sequences");
		for (ArrayList<Integer> seq : lineSequences) {
			for (Integer s : seq)
				System.out.print(s + " ");
			System.out.println("");
		}
		System.out.println("Column sequences");
		for (ArrayList<Integer> seq : columnSequences) {
			for (Integer s : seq)
				System.out.print(s + " ");
			System.out.println("");
		}
	}

	public boolean TLine(Color[][] grid, int line, int j, int l) {
		NullableBoolean[][] cache = new NullableBoolean[j+2][l+1];
		for (int a = 0; a < j+2; a++)
			for (int b = 0; b < l+1; b++)
				cache[a][b] = NullableBoolean.NULL;

		return TLineRec(grid, line, j, l, cache);
	}

	/*
		I splitted the function T() into 2 cases for convenience.
		O(j*l)
	*/
	public boolean TLineRec(Color[][] grid, int line, int j, int l, NullableBoolean[][] cache) {
		if (cache[j+1][l] == NullableBoolean.TRUE)
			return true;
		if (cache[j+1][l] == NullableBoolean.FALSE)
			return false;

		ArrayList<Integer> seq = lineSequences.get(line);
		boolean result;

		if (l == 0) {
			for (int i = 0; i < j + 1; i++) {
				if (grid[line][i] == Color.BLACK) {
					cache[j+1][l] = NullableBoolean.FALSE;
					return false;
				}
			}
			cache[j+1][l] = NullableBoolean.TRUE;
			return true;
		}

		int sl = seq.get(l - 1);
		if (j < sl - 1) {
			cache[j+1][l] = NullableBoolean.FALSE;
			return false;
		}
		else if (j == sl - 1) {
			if (l > 1) {
				cache[j+1][l] = NullableBoolean.FALSE;
				return false;
			}
			else {
				for (int i = 0; i < j + 1; i++) {
					if (grid[line][i] == Color.WHITE) {
						cache[j+1][l] = NullableBoolean.FALSE;
						return false;
					}
				}
				cache[j+1][l] = NullableBoolean.TRUE;
				return true;
			}
		}
		else {
			/**
			int placement = j;
			for (int i = j; i > j-sl; i--) {
				if (grid[line][i]==Color.WHITE) {
					placement = i-1;
					break;
				}
			}
			for (int i = j; i > placement; i--) {
				if (grid[line][i]==Color.BLACK) {
					cache[j+2][l] = NullableBoolean.FALSE;
					return false;
				}
			}
			if (placement-sl+1<0) {
				cache[j+2][l] = NullableBoolean.FALSE;
				return false;
			}
			result = false;
			if (placement-sl<0 || grid[line][placement-sl]!=Color.BLACK) {
				result = TLineRec(grid, line, placement-sl-1, l-1, cache);
			}
			if (result) {
				cache[j+2][l] = NullableBoolean.TRUE;
				return true;
			} else {
				result = TLineRec(grid, line, j-1, l, cache);
				if (result) {
					cache[j+2][l] = NullableBoolean.TRUE;
					return true;
				} else {
					cache[j+2][l] = NullableBoolean.FALSE;
					return false;
				}
			}
			**/
			if (grid[line][j] == Color.BLACK) {
				for (int i = j; i > j - sl; i--) {
					if (grid[line][i] == Color.WHITE) {
							cache[j+1][l] = NullableBoolean.FALSE;
						return false;
					}
				}
				if (grid[line][j - sl] == Color.BLACK) {
					cache[j+1][l] = NullableBoolean.FALSE;
					return false;
				}
				result = TLineRec(grid, line, j - sl - 1, l - 1, cache);
			}
			else if (grid[line][j] == Color.WHITE) {
				result = TLineRec(grid, line, j - 1, l, cache);
			}
			else {
				for (int i = j; i > j - sl; i--) {
					if (grid[line][i] == Color.WHITE) {
						for (int t = j; t > i; t--) {
							if (grid[line][t] == Color.BLACK) {
								for (int u = j+1; u > t; u--)
									cache[u][l] = NullableBoolean.FALSE;
								return false;
							}
						}
						result = TLineRec(grid, line, i - 1, l, cache);	
						if (result) {
							cache[j+1][l] = NullableBoolean.TRUE;
						} else {
							cache[j+1][l] = NullableBoolean.FALSE;
						}
						return result;
					}
				}
				if (grid[line][j - sl] == Color.BLACK)
					result = TLineRec(grid, line, j - 1, l, cache);
				// try black first, probably faster
				else
					result = TLineRec(grid, line, j - sl - 1, l - 1, cache) || TLineRec(grid, line, j - 1, l, cache);
			}
		}
		if (result) {
			cache[j+1][l] = NullableBoolean.TRUE;
		} else {
			cache[j+1][l] = NullableBoolean.FALSE;
		}
		return result;
	}

	public boolean TCol(Color[][] grid, int col, int j, int l) {
		NullableBoolean[][] cache = new NullableBoolean[j+2][l+1];
		for (int a = 0; a < j+2; a++)
			for (int b = 0; b < l+1; b++)
				cache[a][b] = NullableBoolean.NULL;

		return TColRec(grid, col, j, l, cache);
	}

	/*
		O(j*l)
	*/
	public boolean TColRec(Color[][] grid, int col, int j, int l, NullableBoolean[][] cache) {
		//System.out.println("TCol(" + col + ", " + j + ", " + l + ")");
		if (cache[j+1][l] == NullableBoolean.TRUE)
			return true;
		if (cache[j+1][l] == NullableBoolean.FALSE)
			return false;

		ArrayList<Integer> seq = columnSequences.get(col);
		boolean result;

		if (l == 0) {
			for (int i = 0; i < j + 1; i++) {
				if (grid[i][col] == Color.BLACK) {
					cache[j+1][l] = NullableBoolean.FALSE;
					return false;
				}
			}
			cache[j+1][l] = NullableBoolean.TRUE;
			return true;
		}

		int sl = seq.get(l - 1);
		if (j < sl - 1) {
			cache[j+1][l] = NullableBoolean.FALSE;
			return false;
		}
		else if (j == sl - 1) {
			if (l > 1) {
				cache[j+1][l] = NullableBoolean.FALSE;
				return false;
			}
			else {
				for (int i = 0; i < j + 1; i++) {
					if (grid[i][col] == Color.WHITE) {
						cache[j+1][l] = NullableBoolean.FALSE;
						return false;
					}
				}
				cache[j+1][l] = NullableBoolean.TRUE;
				return true;
			}
		}
		else {
			if (grid[j][col] == Color.BLACK) {
				for (int i = j; i > j - sl; i--) {
					if (grid[i][col] == Color.WHITE) {
						cache[j+1][l] = NullableBoolean.FALSE;
						return false;
					}
				}
				if (grid[j - sl][col] == Color.BLACK) {
					cache[j+1][l] = NullableBoolean.FALSE;
					return false;
				}
				result = TColRec(grid, col, j - sl - 1, l - 1, cache);
			}
			else if (grid[j][col] == Color.WHITE) {
				result = TColRec(grid, col, j - 1, l, cache);
			}
			else {
				for (int i = j; i > j - sl; i--) {
					if (grid[i][col] == Color.WHITE) {
						for (int t = j - 1; t > i; t--) {
							if (grid[t][col] == Color.BLACK) {
								for (int u = j+1; u > t; u--)
									cache[u][l] = NullableBoolean.FALSE;
								return false;
							}
						}
						result = TColRec(grid, col, i - 1, l, cache);
						if (result) {
							cache[j+1][l] = NullableBoolean.TRUE;
						} else {
							cache[j+1][l] = NullableBoolean.FALSE;
						}
						return result;
					}
				}
				if (grid[j - sl][col] == Color.BLACK)
					result = TColRec(grid, col, j - 1, l, cache);
				else // try black first, probably faster
					result = TColRec(grid, col, j - sl - 1, l - 1, cache) || TColRec(grid, col, j - 1, l, cache);
			}
		}
		if (result) {
			cache[j+1][l] = NullableBoolean.TRUE;
		} else {
			cache[j+1][l] = NullableBoolean.FALSE;
		}
		return result;
	}

	/*
		O(l*(nbColumns)^2)
	*/
	public boolean colorLine(Color[][] grid, int line, ArrayList<Integer> colonnesAAjouter) {
		for (int j = 0; j < nbColumns; j++) {
			if (grid[line][j] != Color.NOT_COLORED) continue;

			grid[line][j] = Color.WHITE;
			boolean whiteOK = TLine(grid, line, nbColumns - 1, lineSequences.get(line).size());

			grid[line][j] = Color.BLACK;
			boolean blackOK = TLine(grid, line, nbColumns - 1, lineSequences.get(line).size());

			grid[line][j] = Color.NOT_COLORED;

			if (!whiteOK && !blackOK) return false;

			if (whiteOK && !blackOK) {
				grid[line][j] = Color.WHITE;
				colonnesAAjouter.add(j);
			}
			if (!whiteOK && blackOK) {
				grid[line][j] = Color.BLACK;
				colonnesAAjouter.add(j);
			}
		}

		if (filledLine(grid, line))
			if (!verifyLine(grid, line))
				return false;

		return true;
	}

	/*
		O(l*(nbLines)^2)
	*/
	public boolean colorColumn(Color[][] grid, int column, ArrayList<Integer> lignesAAjouter) {
		for (int i = 0; i < nbLines; i++) {
			if (grid[i][column] != Color.NOT_COLORED) continue;

			grid[i][column] = Color.WHITE;
			boolean whiteOK = TCol(grid, column, nbLines - 1, columnSequences.get(column).size());
			
			grid[i][column] = Color.BLACK;
			boolean blackOK = TCol(grid, column, nbLines - 1, columnSequences.get(column).size());
			
			grid[i][column] = Color.NOT_COLORED;

			if (!whiteOK && !blackOK) return false;

			if (whiteOK && !blackOK) {
				grid[i][column] = Color.WHITE;
				lignesAAjouter.add(i);
			}
			if (!whiteOK && blackOK) {
				grid[i][column] = Color.BLACK;
				lignesAAjouter.add(i);
			}
		}

		if (filledColumn(grid, column))
			if (!verifyColumn(grid, column))
				return false;

		return true;
	}

	/*
		O((m*n*m*n*n + m*n*n*m*m)
	*/
	public boolean coloration(Color[][] grid) {
		ArrayList<Integer> lignesAVoir = new ArrayList<Integer>();
		ArrayList<Integer> colonnesAVoir = new ArrayList<Integer>();
		ArrayList<Integer> lignesAAjouter = new ArrayList<Integer>();
		ArrayList<Integer> colonnesAAjouter = new ArrayList<Integer>();
		ArrayList<Integer> lignesASupprimer = new ArrayList<Integer>();
		ArrayList<Integer> colonnesASupprimer = new ArrayList<Integer>();
		boolean ok;

		Color[][] tmpGrid = new Color[nbLines][nbColumns];

		for (int i = 0; i < nbLines; i++)
			for (int j = 0; j < nbColumns; j++)
				tmpGrid[i][j] = grid[i][j];

		for (int i = 0; i < nbLines; i++) {
			lignesAVoir.add(i);
		}

		for (int j = 0; j < nbColumns; j++) {
			colonnesAVoir.add(j);
		}

		while (!lignesAVoir.isEmpty() || !colonnesAVoir.isEmpty()) {
			
			lignesASupprimer.clear();
			colonnesASupprimer.clear();
			colonnesAAjouter.clear();
			lignesAAjouter.clear();

			for (int line : lignesAVoir) {
				ok = colorLine(tmpGrid, line, colonnesAAjouter);
				if (!ok) return false;
				lignesASupprimer.add(Integer.valueOf(line));
			}

			for (int column : colonnesAVoir) {
				ok = colorColumn(tmpGrid, column, lignesAAjouter);
				if (!ok) return false;
				colonnesASupprimer.add(Integer.valueOf(column));
			}

			lignesAVoir.removeAll(lignesASupprimer);
			colonnesAVoir.removeAll(colonnesASupprimer);

			lignesAVoir.addAll(lignesAAjouter);
			colonnesAVoir.addAll(colonnesAAjouter);
		}

		for (int i = 0; i < nbLines; i++)
			for (int j = 0; j < nbColumns; j++)
				this.grid[i][j] = tmpGrid[i][j];

		return filled(this.grid);
	}

	/*
		O(nbColumns)
	*/
	public boolean filledLine(Color[][] grid, int i) {
		for (int j = 0; j < nbColumns; j++)
			if (grid[i][j] == Color.NOT_COLORED)
				return false;
		return true;
	}

	/*
		O(nbLines)
	*/
	public boolean filledColumn(Color[][] grid, int j) {
		for (int i = 0; i < nbLines; i++)
			if (grid[i][j] == Color.NOT_COLORED)
				return false;
		return true;
	}

	/*
		O(nbLines * nbColumns)
	*/
	public boolean filled(Color[][] grid) {
		// colored all
		for (int i = 0; i < nbLines; i++) {
			for (int j = 0; j < nbColumns; j++) {
				if (grid[i][j] == Color.NOT_COLORED) {
					return false;
				}
			}
		}
		return true;
	}

	/*
		O(nbColumns)
	*/
	public boolean verifyLine(Color[][] grid, int i) {
		int iterLine = 0;
		
		boolean blockFlag = false;
		int blockLength = 0;

		ArrayList<Integer> tmp = new ArrayList<Integer>();

		while (iterLine < nbColumns) {
			if (grid[i][iterLine] == Color.BLACK) {
				blockLength++;
				blockFlag = true;
			}
			else {
				if (blockFlag) {
					blockFlag = false;
					tmp.add(blockLength);
					blockLength = 0;
				}
			}
			iterLine++;
		}
		if (blockFlag)
			tmp.add(blockLength);

		if (tmp.size() != lineSequences.get(i).size())
			return false;

		for (int t = 0; t < tmp.size(); t++) {
			if (tmp.get(t) != lineSequences.get(i).get(t))
				return false;
		}
		return true;
	}

	/*
		O(nbLines)
	*/
	public boolean verifyColumn(Color[][] grid, int j) {
		int iterColumn = 0;
		
		boolean blockFlag = false;
		int blockLength = 0;

		ArrayList<Integer> tmp = new ArrayList<Integer>();

		while (iterColumn < nbLines) {
			if (grid[iterColumn][j] == Color.BLACK) {
				blockLength++;
				blockFlag = true;
			}
			else {
				if (blockFlag) {
					blockFlag = false;
					tmp.add(blockLength);
					blockLength = 0;
				}
			}
			iterColumn++;
		}
		if (blockFlag)
			tmp.add(blockLength);

		if (tmp.size() != columnSequences.get(j).size())
			return false;

		for (int t = 0; t < tmp.size(); t++) {
			if (tmp.get(t) != columnSequences.get(j).get(t))
				return false;
		}
		return true;
	}

	/*
		O(nbLines * nbColumns)
	*/
	public boolean verify(Color[][] grid) {

		for (int i = 0; i < nbLines; i++)
			if (!verifyLine(grid, i))
				return false;

		for (int j = 0; j < nbColumns; j++)
			if (!verifyColumn(grid, j))
				return false;

		return true;
	}

	/*
		O(2^(m*n)*P(m,n)) -> O(2^(m*n))
	*/
	public Result enumeration() {

		long start = System.currentTimeMillis();
		boolean attempt = coloration(grid);
		long stop = System.currentTimeMillis();

		System.out.println("Partial method took " + (stop - start) + " milliseconds");
		
		if (attempt) {
			System.out.println("Partial method succeesful.");
			return Result.SUCCEEDED;
		}

		System.out.println("Partial method unsuccessful. Grid obtained so far:");
		displayGrid(grid);

		start = System.currentTimeMillis();

		Color[][] tmpGrid = new Color[nbLines][nbColumns];

		for (int i = 0; i < nbLines; i++)
			for (int j = 0; j < nbColumns; j++)
				tmpGrid[i][j] = grid[i][j];

		Result guessWhite = enumRec(tmpGrid, 0, Color.WHITE);
		
		if (guessWhite == Result.SUCCEEDED) {
			stop = System.currentTimeMillis();
			System.out.println("Complete method took " + (stop - start) + " extra milliseconds");
			return Result.SUCCEEDED;
		}
		
		for (int i = 0; i < nbLines; i++)
			for (int j = 0; j < nbColumns; j++)
				tmpGrid[i][j] = grid[i][j];
		
		Result guessBlack = enumRec(tmpGrid, 0, Color.BLACK);
		stop = System.currentTimeMillis();
		System.out.println("Complete method took " + (stop - start) + " extra milliseconds");
		return guessBlack;
	}

	public Result enumRec(Color[][] grid, int k, Color color) {
		if (k == nbLines * nbColumns)
			return Result.SUCCEEDED;

		while (grid[k/nbColumns][k%nbColumns] != Color.NOT_COLORED) {
			k++;
		}

		Result res = colorGridAndPropagate(grid, k/nbColumns, k%nbColumns, color);

		if (res != Result.DONT_KNOW)
			return res;

		while (grid[k/nbColumns][k%nbColumns] != Color.NOT_COLORED) {
			k++;
		}

		Color[][] tmpGrid = new Color[nbLines][nbColumns];
		
		for (int i = 0; i < nbLines; i++)
			for (int j = 0; j < nbColumns; j++)
				tmpGrid[i][j] = grid[i][j];

		if (enumRec(tmpGrid, k, Color.WHITE) == Result.SUCCEEDED)
			return Result.SUCCEEDED;
		
		for (int i = 0; i < nbLines; i++)
			for (int j = 0; j < nbColumns; j++)
				tmpGrid[i][j] = grid[i][j];

		return enumRec(tmpGrid, k, Color.BLACK);

	}

	public int countRec = 0;
	/*
		meme que coloration() - polynomial en M et N
	*/
	public Result colorGridAndPropagate(Color[][] grid, int i, int j, Color color) {
		countRec++;
		grid[i][j] = color;

		ArrayList<Integer> lignesAVoir = new ArrayList<Integer>();
		ArrayList<Integer> colonnesAVoir = new ArrayList<Integer>();
		ArrayList<Integer> lignesAAjouter = new ArrayList<Integer>();
		ArrayList<Integer> colonnesAAjouter = new ArrayList<Integer>();
		ArrayList<Integer> lignesASupprimer = new ArrayList<Integer>();
		ArrayList<Integer> colonnesASupprimer = new ArrayList<Integer>();
		boolean ok;

		lignesAVoir.add(i);
		colonnesAVoir.add(j);
		
		while (!lignesAVoir.isEmpty() || !colonnesAVoir.isEmpty()) {
			
			lignesASupprimer.clear();
			colonnesASupprimer.clear();
			colonnesAAjouter.clear();
			lignesAAjouter.clear();

			for (int line : lignesAVoir) {
				ok = colorLine(grid, line, colonnesAAjouter);
				if (!ok) {
					return Result.IMPOSSIBLE;
				}
				lignesASupprimer.add(Integer.valueOf(line));
			}

			for (int column : colonnesAVoir) {
				ok = colorColumn(grid, column, lignesAAjouter);
				if (!ok) {
					return Result.IMPOSSIBLE;
				}
				colonnesASupprimer.add(Integer.valueOf(column));
			}

			lignesAVoir.removeAll(lignesASupprimer);
			colonnesAVoir.removeAll(colonnesASupprimer);

			lignesAVoir.addAll(lignesAAjouter);
			colonnesAVoir.addAll(colonnesAAjouter);
		}

		if (filled(grid)) {
			if (verify(grid)) {
				for (int a = 0; a < nbLines; a++)
					for (int b = 0; b < nbColumns; b++)
						this.grid[a][b] = grid[a][b];
				return Result.SUCCEEDED;
			}
			else {
				return Result.IMPOSSIBLE;
			}
		}

		return Result.DONT_KNOW;
	}

	public void displayGrid(Color[][] grid) {
		StringBuilder s = new StringBuilder("Grid of " + nbLines + " lines and " + nbColumns + " columns:\n");
		for (int i = 0; i < nbLines; i++) {
			for (int j = 0; j < nbColumns; j++) {
				if (grid[i][j] == Color.BLACK) {
					s.append("_"); 
				}
				else if (grid[i][j] == Color.WHITE) {
					s.append("W");
				}
				else {
					s.append(" ");
				}
			}
			s.append("\n");
		}
		System.out.println(s.toString());
	}
}