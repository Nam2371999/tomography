import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.lang.Exception.*;
import java.util.ArrayList;

public class Test {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		//new Instance(16).solve();

		long start = System.currentTimeMillis();

		for (int i = 0; i < 16; i++) {
			new Instance(i).solve();
		}

		long stop = System.currentTimeMillis();

		System.out.println("Instances from 1 to 15 " + (stop - start) + " milliseconds");

		start = System.currentTimeMillis();
		new Instance(16).solve();
		stop = System.currentTimeMillis();

		System.out.println("Instance 16 alone took " + (stop - start)/1000 + " seconds");

	}
}