public enum NullableBoolean {
	NULL,
	TRUE,
	FALSE,
}